#  pylint: disable=too-few-public-methods

import builtins
from typing import List, Iterable, Dict, Any, Optional

from pydantic import BaseModel, Field

from app.models.base import Pool
from app.models.bridges import Bridge


class Bridgelines(BaseModel):
    version: str = Field(
        description="Version number of the bridgelines schema in use",
        examples=[
            "1.0"
        ]
    )
    bridgelines: List[str] = Field(
        description="List of bridgelines, ready for use in a torrc file",
        examples=[
            "Bridge obfs4 71.73.124.31:8887 E81B1237F6D13497B166060F55861565593CFF8E "
            "cert=b54NsV6tK1g+LHaThPOTCibdpx3wHm9NFe0PzGF1nwz+4M/tq6SkfOaShzPnZsIRCFRIHg iat-mode=0",
            "Bridge obfs4 172.105.176.101:80 D18BC7E082D7EBF8E851029AC89A12A3F44A50BF "
            "cert=KHfAAUptXWRmLy3ehS9ETMO5luY06d0w7tEBDiAI0z62nC5Qo/APrzZxodkYWX2bNko/Mw iat-mode=0",
            "Bridge obfs4 141.101.36.55:9023 045EF272F08BC11CDB985889E4E9FE35DC6F9C67 "
            "cert=6KEdf/5aDSyuYEqvo14JE8Cks3i7PQtj9EFX2wTCiEaUPsp/I7eaOm4uSWdqwvV4vTVlFw iat-mode=0 "
        ]
    )

    class Config:
        title = "Bridgelines Version 1"


def bridgelines(pool: Pool, *, distribution_method: Optional[str] = None) -> Dict[str, Any]:
    bridges: Iterable[Bridge] = Bridge.query.filter(
        Bridge.destroyed.is_(None),
        Bridge.deprecated.is_(None),
        Bridge.bridgeline.is_not(None)
    ).all()
    if distribution_method is not None:
        bridges = [b for b in bridges
                   if b.conf.distribution_method == distribution_method]
    return Bridgelines(
        version="1.0",
        bridgelines=[b.bridgeline for b in bridges if b.conf.pool_id == pool.id]
    ).dict()


if getattr(builtins, "__sphinx_build__", False):
    schema = Bridgelines.schema_json()
