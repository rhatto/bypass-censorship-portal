from __future__ import annotations

from typing import Any


def is_integer(contender: Any) -> bool:
    """
    Determine if a string (or other object type that can be converted automatically) represents an integer.

    Thanks to https://note.nkmk.me/en/python-check-int-float/.

    :param contender: object to test
    :return: true if it's an integer
    """
    try:
        float(contender)
    except ValueError:
        return False
    else:
        return float(contender).is_integer()
