import enum

from app.brm.brn import BRN
from app.extensions import db
from app.models import AbstractConfiguration


class CloudProvider(enum.Enum):
    AWS = ("aws", "Amazon Web Services")
    AZURE = ("azure", "Microsoft Azure")
    BUNNY = ("bunny", "bunny.net")
    CLOUDFLARE = ("cloudflare", "Cloudflare")
    FASTLY = ("fastly", "Fastly")
    HTTP = ("http", "HTTP")
    GANDI = ("gandi", "Gandi")
    GITHUB = ("github", "GitHub")
    GITLAB = ("gitlab", "GitLab")
    HCLOUD = ("hcloud", "Hetzner Cloud")
    MAXMIND = ("maxmind", "MaxMind")
    OVH = ("ovh", "OVH")
    RFC2136 = ("rfc2136", "RFC2136 DNS Server")

    def __init__(self, key: str, description: str):
        self.key = key
        self.description = description


class CloudAccount(AbstractConfiguration):
    provider = db.Column(db.Enum(CloudProvider))
    credentials = db.Column(db.JSON())
    enabled = db.Column(db.Boolean())
    # CDN Quotas
    max_distributions = db.Column(db.Integer())
    max_sub_distributions = db.Column(db.Integer())
    # Compute Quotas
    max_instances = db.Column(db.Integer())

    bridges = db.relationship("Bridge", back_populates="cloud_account")

    @property
    def brn(self) -> BRN:
        raise NotImplementedError("No BRN for cloud accounts")
