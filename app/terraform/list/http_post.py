from typing import List

from app.terraform.list import ListAutomation


class ListHttpPostAutomation(ListAutomation):
    short_name = "list_http_post"
    description = "Update mirror lists by HTTP POST"
    provider = "http_post"

    template_parameters: List[str] = []

    template = """
    terraform {
      {{ backend_config }}
      required_providers {
        http = {
          version = "~> 3.2.0"
        }
      }
    }

    {% for list in lists %}

    data "http" "post_{{ list.id }}" {
      url                 = "{{ list.container }}"
      method              = "POST"
      request_body = file("{{ list.format }}.{{ list.pool.pool_name }}.{{ list.encoding }}")

      request_headers = {
        Authorization = "Bearer {{ list.filename }}"
        Content-Type = "application/json"
      }
    }
    {% endfor %}
    """
