import logging
from abc import ABC
from typing import List

from app.terraform.block.bridge import BlockBridgeAutomation


class BlockBridgelinesAutomation(BlockBridgeAutomation, ABC):

    _lines: List[str]

    def parse(self) -> None:
        for line in self._lines:
            parts = line.split(" ")
            try:
                ip_address = parts[1].split(":")[0]
                fingerprint = parts[2]
                self.ips.append(ip_address)
                self.fingerprints.append(fingerprint)
                logging.debug(f"Added blocked bridge with IP {ip_address} and fingerprint {fingerprint}")
            except IndexError:
                logging.warning("A parsing error occured.")
