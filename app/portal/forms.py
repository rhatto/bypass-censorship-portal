from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, SelectField


class EditMirrorForm(FlaskForm):  # type: ignore
    origin = SelectField('Origin')
    url = StringField('URL')
    submit = SubmitField('Save Changes')
