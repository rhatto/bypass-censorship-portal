import json

from nose.tools import assert_equal, assert_not_in

from app.terraform.list import obfuscator


def test_obfuscating_string():
    data = "hello"
    obfs = obfuscator(data)
    print(f"Obfuscated string: {obfs}")
    j = json.dumps(obfs).replace("!AAA!", "\\u")
    loaded = json.loads(j)
    assert_equal(data, loaded)


def test_obfuscating_simple_dict():
    data = {"hello": "world"}
    obfs = obfuscator(data)
    print(f"Obfuscated string: {obfs}")
    j = json.dumps(obfs).replace("!AAA!", "\\u")
    assert_not_in("hello", obfs)
    assert_not_in("world", obfs)
    loaded = json.loads(j)
    assert_equal(data, loaded)


def test_obfuscating_simple_list():
    data = ["hello", "world"]
    obfs = obfuscator(data)
    print(f"Obfuscated string: {obfs}")
    j = json.dumps(obfs).replace("!AAA!", "\\u")
    assert_not_in("hello", obfs)
    assert_not_in("world", obfs)
    loaded = json.loads(j)
    assert_equal(data, loaded)


def test_obfuscating_for_real():
    data = json.load(open("tests/list/mirrorSites.json"))
    obfs = obfuscator(data)
    j = json.dumps(obfs).replace("!AAA!", "\\u")
    print(f"Obfuscated string: {obfs}")
    for a in range(17, 27):
        assert_not_in(chr(a), j)
    loaded = json.loads(j)
    assert_equal(data, loaded)
