Mirror List Formats
===================

Bypass Censorship Extension
---------------------------

.. jsonschema:: app.lists.bc2.schema

Mirror Analytics
----------------

.. jsonschema:: app.lists.mirror_mapping.schema

Tor Bridges
-----------

.. jsonschema:: app.lists.bridgelines.schema