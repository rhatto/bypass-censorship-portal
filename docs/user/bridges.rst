Tor Bridges
===========

Background
----------

When someone uses the Tor network to browse the internet, their traffic is routed through a series of relays, making it
difficult to trace the origin of the traffic or the content being accessed. However, in some countries or networks,
access to the Tor network is blocked, making it impossible to use the network.

A Tor bridge, a special type of node on the Tor network that is not listed in the public directory of nodes, is designed
to help users in these situations.
Instead of connecting directly to the Tor network, a user can connect to a Tor bridge first.
The bridge will then forward the user's traffic to the Tor network, effectively bypassing any censorship or blocking.
This makes it harder for governments and other organizations to block access to the Tor network, because users can
still connect to the network through these unlisted bridges.

Pluggable Transports
""""""""""""""""""""

A key feature of a Tor bridge is its "obfuscation" capability, provided by pluggable transports.
This means that the bridge's traffic is disguised in a way that makes it difficult for censors to detect that it is Tor
traffic through deep packet inspection (DPI).
For example, the bridge may use a different network protocol or port than standard Tor traffic, making it harder for
censors to identify and block it.

A pluggable transport allows users to obfuscate their Tor traffic using a variety of different protocols and techniques.
Different pluggable transports will have different levels of success in different censorship environments, and new
techniques are always being designed and studied. The portal currently only supports deploying bridges using the
obfs4proxy pluggable transport.

Distribution Methods
""""""""""""""""""""

`BridgeDB <https://bridges.torproject.org/>`_, and its upcoming replacement rdsys, is a service used by the Tor network
to distribute bridge addresses to users who need them.
If you're running bridges for the general public to use then BridgeDB will help users get hold of the details for your
bridges.
Unless you have specific requirements, you can let BridgeDB determine the best distribution method for your
bridge by choosing the "Any" method.

Within each distribution method the bridges are further split up into *pools* (a concept borrowed by the portal too,
see: *Resource Pools*). In some countries or networks, censors may attempt to block all known bridge addresses
associated with the Tor network. Depending on criteria such as geography, your ISP, and other secret factors, each
user request for a bridge is mapped to one of these pools.
The aim is to prevent any censor from being able to discover all of the bridges, leaving bridges unblocked for other
legitimate users.

If you already have a channel to your users then you should select the "None" BridgeDB distribution method to
ensure that your bridges remain private and only for use by your community.

Managing Configurations
-----------------------

As with other circumvention resources, the dashboard takes the approach of managing collections of resources that are
uniformly configured and expendable. For this reason the only configuration that can be performed is at a high level
with the management of individual resources taking place through the automation subsystem.

Bridges will be deployed to all available cloud providers, although you can disable providers by setting a zero instance
limit within the Cloud Account configuration.

Once your administrator has provided you access to the portal, you can begin to configure your Tor Bridge deployments.
To get started, select "Tor Bridges" under "Configuration" from the menu on the left hand side. If you are using a
mobile device, you may need to click the hamburger icon at the top of the screen to open the menu.

.. image:: /_static/bridges/list.png
   :width: 800

New Configuration
-----------------

To create a new configuration, click "Create new configuration" at the top of the configuration list. This will present
you with the new configuration form:

.. image:: /_static/bridges/new.png
   :width: 800

Distribution Method
"""""""""""""""""""

The distribution method for `BridgeDB <https://bridges.torproject.org/>`_.
Unless you have specific requirements you will likely choose either "Any" to allow for BridgeDB to allocate the bridge
to where it is most needed, or "None" to have a bridge that is not distributed by BridgeDB for you to distribute
directly to the end-user via another channel.

Description
"""""""""""

A free-form text description to help identify the collection.

Pool
""""

The Resource Pool the bridges in this configuration belong to.
If you distribute your bridges by Distribution List within the portal, this will allow you to split up which bridges
are distributed.

Target Number
"""""""""""""

The target number of active bridges to have deployed at any time, excluding deprecated bridges. When editing, increasing
or decreasing this number will cause new bridges to be created, or existing bridges to be destroyed, so that the number
deployed is less than the maximum number while having as close as possible to the target number of non-deprecated
bridges.

Maximum Number
""""""""""""""

The maximum number of bridges to deploy including deprecated bridges.

Expiry Timer
""""""""""""

The number of hours to wait after a bridge is deprecated before its destruction. It is not advisable to set this number
to zero as this does not allow any churn of IP address usage within the cloud provider and you are likely to get the
same IP address back for the new bridge, leading it to arriving dead on arrival. It is also not advisable to set this
number too high as this will result in cost incurred for a bridge that may not be able to be used. Remember that just
because the bridge is blocked in one network, it may remain accessible from other networks, and so it is not instantly
worthless when the first block is detected (with the exception perhaps of very specific community use cases).

Provider Allocation Method
""""""""""""""""""""""""""

Two provider allocation methods have been implemented:

* Use cheapest provider first - based on the prevailing cost of a Tor bridge at each provider, deploy on the cheapest
  provider first until the quota is fully utilised, and then proceed to deploy on other providers in order of cost.
* Use providers randomly - each bridge deployed will go to a random choice of cloud provider (RNG is not cryptographic).

Edit Configuration
------------------

.. image:: /_static/bridges/edit.png
   :width: 800

See the "New Configuration" section above for the descriptions of each field.

Once a configuration has been created, the Resource Pool and Distribution Method may not be modified. This is because
the bridges may already have been distributed by some method, and rather than reallocating the existing bridges to a
new Distribution Method or Resource Pool it would be more effective to destroy the configuration and then create a new
configuration with new bridges for those new users.

Destroy Configuration
---------------------

.. image:: /_static/bridges/destroy.png
   :width: 800

When destroying a configuration, the bridges deployed for that configuration will be automatically destroyed with it.

Bridge Rotation
---------------

.. image:: /_static/bridges/block.png
   :width: 800

If not using an automated block detection and replacement system, bridges can be manually replaced as needed.
When viewing the bridge list, either while editing the configuration or the full list of bridges, select "Mark as
blocked".
The bridge will be replaced on the next automation run.
