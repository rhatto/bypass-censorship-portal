#FROM python:3.9.13-slim-bullseye
FROM debian:bullseye AS portal
MAINTAINER Ana Custura <ana@sr2.uk>

ENV APP="bc"
ENV APP_BASE="/srv/"
ENV SHELL="/bin/bash"
ENV FLASK_APP="${FLASK_APP:-app}"
ENV FLASK_RUN_HOST="${FLASK_RUN_HOST:-0.0.0.0}"
ENV FLASK_RUN_PORT="${FLASK_RUN_PORT:-5000}"

# Set PATH and PYTHONPATH in the container
ENV PYTHONPATH="/usr/lib/python3/dist-packages:/home/${APP}/.local/lib/python3.9/site-packages"
ENV PATH="/usr/local/bin:/usr/bin:/bin:/sbin:/usr/sbin:/home/${APP}/.local/bin"

# UID and GID might be read-only values, so use non-conflicting ones
ARG CONTAINER_UID="${CONTAINER_UID:-1000}"
ARG CONTAINER_GID="${CONTAINER_GID:-1000}"

# Install dependencies
RUN apt-get update && \
    apt-get install --no-install-recommends -y \
    curl \
    software-properties-common \
    python3-pip \
    cron \
    gnupg2

# Install Terraform
# See https://www.terraform.io/downloads
RUN /usr/bin/curl -fsSL https://apt.releases.hashicorp.com/gpg | apt-key add -
RUN apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com bullseye main"
RUN apt-get update && \
    apt-get install -y terraform \
    && rm -rf /var/lib/apt/lists/*

# Switch to a regular user
RUN groupadd -r -g ${CONTAINER_GID} ${APP} && \
    useradd --no-log-init -r -u ${CONTAINER_UID} -g ${APP} ${APP} && \
    mkdir -p /home/${APP} && chown -R ${APP}. /home/${APP}
RUN mkdir -p ${APP_BASE}/${APP} && chown ${APP}. ${APP_BASE}/${APP}
USER ${APP}

# Copy the project into the workdir
WORKDIR ${APP_BASE}/${APP}
COPY . ${APP_BASE}/${APP}

# Install Python requirements
RUN pip3 install -r requirements.txt
RUN pip3 install psycopg2-binary

USER root
RUN crontab -u ${APP} docker-crontab
# Set the entrypoint to the web app
