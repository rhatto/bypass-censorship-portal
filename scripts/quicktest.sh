#!/bin/sh

set -e

bandit -r app
flake8 app
mypy app
nosetests tests

